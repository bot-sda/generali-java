# Maven course

# Introduction to Maven
Maven is a build automation tool used for building and managing any Java-based project. It simplifies the process of building and managing a project by providing a uniform build system, standard project layout, and project information. In this course, you will learn the basics of Maven and how to use it to build and manage Java projects.

## Prerequisites
Before starting this course, you should have some basic knowledge of Java programming, and be familiar with the command line interface.

## Topics to be covered
This course will cover the following topics:

1. Maven basics
2. Maven installation and configuration
3. Creating a new Maven project
4. Adding dependencies to a Maven project
5. Building and packaging a Maven project
6. Managing Maven project lifecycle
7. Maven plugins
8. Maven repositories
9. Advanced Maven topics


# Section 1: Maven Basics
## What is Maven?
Maven is a build automation tool used to create and manage Java projects. It provides a standard project structure, dependency management, and a build lifecycle. Maven is based on the concept of a Project Object Model (POM), which is an XML file that contains information about the project and its dependencies.

## Why use Maven?
Maven simplifies the build process and makes it easier to manage dependencies. It also provides a standard way to build projects, making it easier for developers to work on different projects without having to learn a new build system for each project.

## Maven Terminology
Here are some Maven terminologies you should know:

- **Project**: A Maven project is a collection of source files, configuration files, and resources that make up an application or library.
- **Artifact**: An artifact is a file produced by Maven during the build process. It can be a JAR, WAR, or any other file type.
- **Dependency**: A dependency is a library or module that is required by the project to compile or run.
- **Plugin**: A Maven plugin is a tool that extends the functionality of Maven. Plugins can be used to perform specific tasks during the build process, such as compiling code, generating documentation, or deploying artifacts.

# Section 2: Maven Installation and Configuration
## Installing Maven
To install Maven, follow these steps:

1. Download the latest version of Maven from the Apache Maven website (https://maven.apache.org/download.cgi).
2. Extract the contents of the downloaded file to a directory on your computer.
3. Set the M2_HOME environment variable to the directory where Maven was installed.
4. Add the Maven bin directory to your system's PATH environment variable.
# Configuring Maven
To configure Maven, you can edit the **settings.xml** file located in the **conf** directory of your Maven installation. This file contains settings such as the location of the local repository, the proxy server settings, and the Maven repository configuration.

# Section 3: Creating a new Maven project
To create a new Maven project, follow these steps:

1. Open a command prompt or terminal window.
2. Change to the directory where you want to create the new project.
3. Run the following command: **mvn archetype:generate**
4. Follow the prompts to select the project type and enter the project information.

# Section 4: Adding dependencies to a Maven project
To add dependencies to a Maven project, you need to specify them in the project's POM file. Here's an example of how to add a dependency to a project:

```
<dependencies>
  <dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-core</artifactId>
    <version>5.1.0.RELEASE</version>
  </dependency>
</dependencies>
```

In the above example, we are adding the **spring-core** library as a dependency to our project.

# Section 5: Building and packaging a Maven project
To build and package a Maven project, follow these steps:

1. Open a command prompt or terminal window.
2. Change to the directory where the project is located.
3. Run the following command: **mvn clean package**
4. Maven will compile the source code, run tests, and package the project into an artifact such as a JAR or WAR file.

# Section 6: Managing Maven project lifecycle
Maven defines a standard lifecycle for building and packaging projects. The default lifecycle includes the following phases:

- **validate**: validate the project is correct and all necessary information is available.
- **compile**: compile the source code of the project.
- **test**: test the compiled source code using a suitable unit testing framework.
- **package**: take the compiled code and package it in its distributable format, such as a JAR.
- **verify**: run any checks on results of integration tests to ensure quality criteria are met.
- **install**: install the package into the local repository, for use as a dependency in other projects locally.
- **deploy**: done in an integration or release environment, copies the final package to the remote repository for sharing with other developers and projects.
You can run these phases by invoking the corresponding Maven goals. For example, to run the **test** phase, you can run the following command: **mvn test**

# Section 7: Maven plugins
Maven plugins are tools that extend the functionality of Maven. They can be used to perform specific tasks during the build process, such as compiling code, generating documentation, or deploying artifacts. Maven provides a number of built-in plugins, and you can also create your own custom plugins.

Here's an example of how to use the Maven compiler plugin to compile the source code of a project:

```
<build>
  <plugins>
    <plugin>
      <groupId>org.apache.maven.plugins</groupId>
      <artifactId>maven-compiler-plugin</artifactId>
      <version>3.8.0</version>
      <configuration>
        <source>1.8</source>
        <target>1.8</target>
      </configuration>
    </plugin>
  </plugins>
</build>
```

In the above example, we are configuring the Maven compiler plugin to use Java 1.8 as the source and target version.


# Section 8: Maven repositories
Maven repositories are collections of binary artifacts and metadata that can be used by Maven to resolve dependencies. There are two types of repositories: local repositories and remote repositories.

## Local repositories
A local repository is a repository located on your local machine where Maven stores artifacts that are downloaded from remote repositories. By default, the local repository is located in the **.m2** directory in your home directory.

## Maven Central

Maven Central (https://mvnrepository.com/) is the central repository for all Maven artifacts, making it an essential resource for software developers using Maven. This repository contains a vast collection of pre-built libraries and components that developers can use to quickly and easily build their own software projects. Maven Central provides a central location for developers to share their code and ensure that it is easily accessible to others, creating a vibrant and thriving ecosystem of open source development.

Developers can use Maven Central to search for and download dependencies for their projects. Maven Central also serves as a distribution point for project releases, ensuring that users have access to the latest versions of the software they need.

One of the primary benefits of Maven Central is that it eliminates the need for developers to manually download and manage external libraries and dependencies. Instead, they can simply declare their dependencies in their POM file and let Maven handle the rest. This streamlines the development process and allows developers to focus on writing high-quality code rather than managing dependencies.

## Remote repositories
A remote repository is a repository located on a remote server that can be used by Maven to download dependencies. Maven comes with a number of built-in remote repositories, and you can also configure your own custom remote repositories.

```
<project>
  ...
  <repositories>
    <repository>
      <id>my-remote-repository</id>
      <url>http://my-repository-server.com/repository</url>
      <releases>
        <enabled>true</enabled>
      </releases>
      <snapshots>
        <enabled>true</enabled>
      </snapshots>
      <authentication>
        <username>my-username</username>
        <password>my-password</password>
      </authentication>
    </repository>
  </repositories>
  ...
</project>
```

In this example, we are configuring a remote repository with ID **my-remote-repository** that points to a URL of **http://my-repository-server.com/repository**. We have enabled both release and snapshot versions of artifacts in this repository. Additionally, we have included an **<authentication>** element that specifies a username and password to use when accessing the repository.

When you run a command that requires access to the remote repository, such as **mvn install**, Maven will prompt you for the username and password specified in the **<authentication>** element. Alternatively, you can specify these values on the command line using the **-Dusername** and **-Dpassword** options, like this:

```
mvn install -Dusername=my-username -Dpassword=my-password
```

Note that storing authentication credentials in plain text in your **pom.xml** file is not recommended, as it can compromise the security of your repository. Instead, you can use Maven's **settings.xml** file to store encrypted credentials, or use a tool like Sonatype Nexus or JFrog Artifactory to manage repository access more securely.

# Section 9: Advanced Maven topics
Here are some advanced topics you can explore to learn more about Maven:

- **Multi-module projects**: Maven supports the creation of multi-module projects, which are projects that contain multiple modules that can be built independently or as a group.
- **Maven profiles**: Maven profiles allow you to define different build configurations for different environments or situations.
- **Maven archetypes**: Maven archetypes are templates for creating new projects. They provide a pre-configured project structure and can be used to quickly create new projects with the required dependencies